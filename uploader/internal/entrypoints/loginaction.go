package entrypoints

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"html/template"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"
)

func (ep Entrypoints) login(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	login := r.FormValue("login")
	ep.logger.Info("login:", login)
	password := r.FormValue("password")
	ep.logger.Info("password: ", password)
	expiration, hashedToken, err := ep.usecases.CheckLogin(login, password)
	if err == nil {
		cookie := http.Cookie{Name: "token", Value: url.QueryEscape(hashedToken), Expires: expiration}
		http.SetCookie(rw, &cookie)
		http.Redirect(rw, r, "/", http.StatusSeeOther)
	} else {
		ep.loginPage(rw, err.Error())
	}
}

func (ep Entrypoints) loginPage(rw http.ResponseWriter, message string) {
	lp := filepath.Join("static", "login.html")
	tmpl, err := template.ParseFiles(lp)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	type answer struct {
		Message string
	}
	data := answer{message}
	err = tmpl.ExecuteTemplate(rw, "login", data)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
}

func (ep Entrypoints) logout(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	for _, v := range r.Cookies() {
		c := http.Cookie{
			Name:   v.Name,
			MaxAge: -1}
		http.SetCookie(rw, &c)
	}
	http.Redirect(rw, r, "/login", http.StatusSeeOther)
}

func (ep Entrypoints) signupPage(rw http.ResponseWriter, message string) {
	sp := filepath.Join("static", "signup.html")
	tmpl, err := template.ParseFiles(sp)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	type answer struct {
		Message string
	}
	data := answer{message}
	err = tmpl.ExecuteTemplate(rw, "signup", data)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}

}

func (ep Entrypoints) Signup(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	name := strings.TrimSpace(r.FormValue("name"))
	surname := strings.TrimSpace(r.FormValue("surname"))
	login := strings.TrimSpace(r.FormValue("login"))
	password := strings.TrimSpace(r.FormValue("password"))
	password2 := strings.TrimSpace(r.FormValue("password2"))
	createdUser, err := ep.usecases.SignUp(name, surname, login, password, password2)
	if err != nil {
		ep.signupPage(rw, fmt.Sprintf("Ошибка создания пользователя: %v", err))
		return
	}
	ep.loginPage(rw, fmt.Sprintf("%s, вы успешно зарегистрировались! Теперь вы имеет доступ через страницу авторизации", createdUser.Name))
}
