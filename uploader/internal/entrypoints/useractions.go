package entrypoints

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func (ep *Entrypoints) Authorized(next httprouter.Handle) httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		token, err := readCookie("token", r)
		if err != nil {
			http.Redirect(rw, r, "/login", http.StatusSeeOther)
			return
		}
		user, ok := ep.cache[token]
		ep.logger.Info("User id from cache ", fmt.Sprint(user.Id))
		if !ok {
			http.Redirect(rw, r, "/login", http.StatusSeeOther)
			return
		}
		userid := httprouter.Param{Key: "userid", Value: fmt.Sprint(user.Id)}
		ep.logger.Info("User id passed by auth", userid)
		ps = append(ps, userid)
		ep.logger.Info("User id passed by auth", userid)
		next(rw, r, ps)
	}
}
