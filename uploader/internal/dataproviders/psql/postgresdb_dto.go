package psql

import (
	"time"
)

type User struct {
	Id             int    `json:"id" gorm:"primary_key,uniq"`
	Login          string `json:"login" gorm:"login,uniq"`
	HashedPassword string `json:"hashed_password" db:"hashed_password"`
	VKuser         string `json:"vkuser"`
	VKpass         string `json:"vkpass"`
	VKgroups       string `json:"vkgroups"`
	TelegramToken  string `json:"telegramtoken"`
	TelegramGroups string `json:"telegramgroups"`
	Name           string `json:"name" db:"name"`
	Surname        string `json:"surname" db:"surname"`
}

type Post struct {
	Id         int       `json:"id" gorm:"primary_key,uniq"`
	OwnerId    int       `json:"ownerid" gorm:"foreignKey:ID"`
	TimeOfPost time.Time `json:"time" gorm:"autoCreateTime"`
	Text       string    `json:"text"`
	PhotoPath  string    `json:"photopath"`
	Daily      bool      `json:"daily"`
	Monthly    bool      `json:"monthly"`
	Done       bool      `json:"done"`
}
