package usecases

import (
	"io"
	"os"
	"strconv"
	"time"
	"uploader/internal/dataproviders/psql"
	"uploader/utils"
)

func (uc *UseCases) GetUserPostList(user psql.User) ([]psql.Post, error) {
	postList, err := uc.psqlprovider.GetAllPosts(user)
	if err != nil {
		uc.logger.Error(err)
		return nil, err
	}
	return postList, nil
}
func (uc *UseCases) CreatePost(user psql.User, postdatetime, text string, daily, monthly bool, file io.Reader) error {
	publicationTime, err := time.Parse("2006-01-02T15:04", postdatetime)
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempFile, err := os.CreateTemp(utils.Env.ImagePath, "upload-*.png")
	if err != nil {
		uc.logger.Error(err)
		return err
	}

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := io.ReadAll(file)
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)
	uc.logger.Info(tempFile.Name())
	tempFile.Close()
	// return that we have successfully uploaded our file!
	_, err = uc.psqlprovider.CreatePost(&psql.Post{
		OwnerId:    user.Id,
		TimeOfPost: publicationTime,
		PhotoPath:  tempFile.Name(),
		Text:       text,
		Daily:      daily,
		Monthly:    monthly,
	})
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	return nil
}

func (uc *UseCases) UpdatePost(idstring, postdatetime, text string, daily, monthly bool, file io.Reader) error {
	id, err := strconv.Atoi(idstring)
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	// В случае каких либо ошибок система берет значения из уже существующей записи.
	postUpdate := psql.Post{Id: id}
	oldpost, err := uc.GetPost(psql.Post{Id: id})
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	// Проверка для времени
	publicationTime, err := time.Parse("2006-01-02T15:04", postdatetime)
	if err != nil {
		uc.logger.Info("Couldn't get publication time, geting old one: ", err)
		postUpdate.TimeOfPost = oldpost[0].TimeOfPost
	} else {
		postUpdate.TimeOfPost = publicationTime
	}
	// Проверка файла
	if file != nil {
		tempFile, err := os.CreateTemp(utils.Env.ImagePath, "upload-*.png")
		if err != nil {
			uc.logger.Error(err)
			return err
		}
		fileBytes, err := io.ReadAll(file)
		_, err = tempFile.Write(fileBytes)
		if err != nil {
			uc.logger.Error(err)
			return err
		}
		uc.logger.Info(tempFile.Name())
		err = tempFile.Close()
		if err != nil {
			uc.logger.Error(err)
			return err
		}
		postUpdate.PhotoPath = tempFile.Name()
		err = os.Remove(oldpost[0].PhotoPath)
		if err != nil {
			uc.logger.Error("Pic delete error: ", err)
		}
	} else {
		uc.logger.Info("No pic update for record")
		postUpdate.PhotoPath = oldpost[0].PhotoPath
	}

	if text == "" {
		postUpdate.Text = oldpost[0].Text
	} else {
		postUpdate.Text = text
	}
	if daily != postUpdate.Daily {
		postUpdate.Daily = daily
	}
	if monthly != postUpdate.Monthly {
		postUpdate.Monthly = monthly
	}

	//Обновление поста
	_, err = uc.psqlprovider.UpdatePost(postUpdate)
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	return nil
}

func (uc *UseCases) GetPost(post psql.Post) ([]psql.Post, error) {
	postFromDb, err := uc.psqlprovider.GetPosts(post)
	if err != nil {
		return []psql.Post{}, err
	}
	return postFromDb, nil
}
func (uc *UseCases) DeletePost(post *psql.Post) error {
	err := uc.psqlprovider.DeletePost(*post)
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	return nil
}
