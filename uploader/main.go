package main

import (
	"context"
	"go.uber.org/zap"
	"uploader/internal/dataproviders/psql"
	"uploader/internal/entrypoints"
	"uploader/internal/usecases"
	"uploader/utils"
)

var (
	ctx          context.Context
	sugar        *zap.SugaredLogger
	Psqlprovider psql.IPostgresProvider
	Entrypoints  entrypoints.IEntrypoints
	Usecases     usecases.IUseCases
	cache        map[string]psql.User
)

func init() {
	utils.Env = utils.Getenv()
	cache = make(map[string]psql.User)
	logger, _ := zap.NewProduction()
	defer logger.Sync() // flushes buffer, if any
	sugar = logger.Sugar()
	Psqlprovider = psql.NewPostgresProvider(ctx, sugar)
	Psqlprovider.InitDB()
	Usecases = usecases.NewUseCases(ctx, Psqlprovider, sugar, cache)
	Entrypoints = entrypoints.NewEntrypoints(ctx, Usecases, sugar, cache)
}

func main() {

	sugar.Info("Hello World")
	Entrypoints.SetupRoutes()
}
