package main

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	vkapi "github.com/himidori/golang-vk-api"
	"go.uber.org/zap"
	"poster/internal/dataproviders/psql"
	"poster/internal/entrypoints"
	"poster/internal/usecases"
	"poster/utils"
	"time"
)

var (
	ctx          context.Context
	Entrypoints  entrypoints.IEntrypoints
	Usecases     usecases.IUseCases
	Psqlprovider psql.IPostgresProvider
	sugar        *zap.SugaredLogger
	logger       *zap.Logger
)

func init() {
	utils.Env = utils.Getenv()
	if utils.Env.LOG_LEVEL == "PROD" {
		logger, _ = zap.NewProduction()
	} else {
		logger, _ = zap.NewDevelopment()
	}
	defer logger.Sync() // flushes buffer, if any
	sugar = logger.Sugar()
	Psqlprovider = psql.NewPostgresProvider(ctx, sugar)
	Psqlprovider.InitDB()
	Usecases = usecases.NewUseCases(ctx, Psqlprovider, sugar)
	Entrypoints = entrypoints.NewEntrypoints(ctx, Usecases, Psqlprovider, sugar)
	utils.VkConnectors = make(map[int]*vkapi.VKClient)
	utils.TelegramBots = make(map[int]*tgbotapi.BotAPI)
}

func main() {
	sugar.Info("Worker started")

	for {
		sugar.Info("Publication")
		Entrypoints.PostPublicator()
		time.Sleep(time.Second * 30)
	}

	//TODO Перенести vkprovider and TelegramProvider
	//listenerChan := make(chan bool)
	//existChan := make(chan bool)

	//Проверяем подключение к vk
	//go func(exitChan chan bool) {
	//	defer func() {
	//		existChan <- true
	//	}()
	//	err := VkProvider.Reconnector(&utils.Env)
	//	if err != nil {
	//		Sugar.Error(err)
	//	}
	//}(listenerChan)

	////Проверяем подключение к telegram
	//go func(exitChan chan bool) {
	//	defer func() {
	//		existChan <- true
	//	}()
	//	err := TelegramProvider.Reconnector(&utils.Env)
	//	if err != nil {
	//		Sugar.Error(err)
	//	}
	//}(listenerChan)

	//Публикуем по времени
	//go func(exitChan chan bool) {
	//	defer func() {
	//		existChan <- true
	//	}()
	//}(listenerChan)
	//<-listenerChan
}
