module poster

go 1.16

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/himidori/golang-vk-api v0.0.0-20210404104913-eff438684eb7
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.2
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa // indirect
	gorm.io/driver/postgres v1.3.8
	gorm.io/gorm v1.23.8
)
