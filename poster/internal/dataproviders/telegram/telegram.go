package telegram

import (
	"context"
	"errors"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"go.uber.org/zap"
	"poster/internal/dataproviders/psql"
	"poster/utils"
	"strconv"
)

type TelegramApiProvider struct {
	ctx    context.Context
	logger *zap.SugaredLogger
}

type ITelegramApiProvider interface {
	Reconnector(user *psql.User) error
	PostMessage(user *psql.User, post *psql.Post) error
}

func NewTelegramApiProvider(
	ctx context.Context,
	sugaredLogger *zap.SugaredLogger,
) ITelegramApiProvider {
	return &TelegramApiProvider{
		ctx:    ctx,
		logger: sugaredLogger,
	}
}

func (it *TelegramApiProvider) botConnector(user *psql.User) (*tgbotapi.BotAPI, error) {
	if utils.TelegramBots[user.Id] == nil {
		// используя токен создаем новый инстанс бота
		Bot, err := tgbotapi.NewBotAPI(user.TelegramToken)
		if err != nil {
			switch err.Error() {
			case "Not Found":
				it.logger.Info("Disabling telegram bot")
				return nil, err
			default:
				it.logger.Error("telegram not connected, check internet connection")
				return nil, err
			}
		} else {
			it.logger.Infof("Authorized as bot: %s", Bot.Self.UserName)
			return Bot, nil
		}
	}
	it.logger.Info("Telegram connector is actual")
	return utils.TelegramBots[user.Id], nil
}
func (it *TelegramApiProvider) Reconnector(user *psql.User) error {
	it.logger.Infof("telegram cache has %d connectors", len(utils.TelegramBots))
	if user.TelegramToken != "" || user.TelegramGroups != "" {
		it.logger.Info("Is telegram connector actual?")
		connection, err := it.botConnector(user)
		if err != nil {
			return err
		}
		utils.TelegramBots[user.Id] = connection
		it.logger.Info("Now telegram connector is actual")
		return nil
	} else {
		return errors.New("Teleram credentials not completed, disabling telegramposter poster")
	}
}

func (it *TelegramApiProvider) PostMessage(user *psql.User, post *psql.Post) error {
	it.logger.Info("Posting message")
	telegramGroupId, err := strconv.Atoi(user.TelegramGroups)
	if err != nil {
		return err
	}
	//if len(post.PhotoPath) == 1 {
	it.logger.Info("Amount of photo = 1")

	publication := tgbotapi.NewPhoto(
		int64(telegramGroupId),
		tgbotapi.FilePath(post.PhotoPath))
	publication.Caption = post.Text
	message, err := utils.TelegramBots[user.Id].Send(publication)
	if err != nil {
		it.logger.Info("Send error")
		return err
	}
	it.logger.Info(message)
	//} else {
	//	it.logger.Infof("Amount of photo %d", len(post.PhotoPath))
	//	var mediaPhotoArrayToPost []interface{}
	//	for i, path := range post.PhotoPath {
	//		if i > 9 {
	//			break
	//		}
	//		data := tgbotapi.FilePath(path)
	//		tempPhoto := tgbotapi.NewInputMediaPhoto(data)
	//		mediaPhotoArrayToPost = append(mediaPhotoArrayToPost, tempPhoto)
	//	}
	//	_, err := utils.TelegramBots[user.Id].Send(
	//		tgbotapi.NewMediaGroup(
	//			int64(telegramGroupId),
	//			mediaPhotoArrayToPost))
	//	if err != nil {
	//		return err
	//	}
	//	_, err = utils.TelegramBots[user.Id].Send(
	//		tgbotapi.NewMessage(
	//			int64(telegramGroupId),
	//			post.Text))
	//	if err != nil {
	//		return err
	//	}
	//}
	return nil
}
