package vk

import (
	"context"
	"errors"
	vkapi "github.com/himidori/golang-vk-api"
	"go.uber.org/zap"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"poster/internal/dataproviders/psql"
	"poster/utils"
	"strconv"
	"strings"
)

type VkApiProvider struct {
	ctx    context.Context
	logger *zap.SugaredLogger
}

type IVkApiProvider interface {
	Reconnector(user *psql.User) error
	PostGroupPhoto(user *psql.User, post *psql.Post) error
}

func NewVkApiProvider(
	ctx context.Context,
	sugaredlogger *zap.SugaredLogger,
) IVkApiProvider {
	return &VkApiProvider{
		ctx:    ctx,
		logger: sugaredlogger,
	}
}

// Проверяем подключение к апи вк.
func (vap *VkApiProvider) enterTheMatrix(u *psql.User) *vkapi.VKClient {
	if utils.VkConnectors[u.Id] != nil {
		vap.logger.Info("VK connector is actual")
		return utils.VkConnectors[u.Id]
	} else {
		Connector, err := vkapi.NewVKClient(vkapi.DeviceIPhone, u.VKuser, u.VKpass, true)
		if err != nil {
			switch err.Error() {
			case "invalid_client: Username or password is incorrect":
				vap.logger.Error("disabling vk")
				return nil
			default:
				vap.logger.Error("Vk not connected, check internet connection")
				return nil
			}
		} else {
			vap.logger.Info("VK connector is created")
			return Connector
		}
	}
}

func (vap *VkApiProvider) Reconnector(user *psql.User) error {
	vap.logger.Infof("vk cache has %d connectors", len(utils.VkConnectors))
	if user.VKuser != "" || user.VKpass != "" {
		vap.logger.Info("Is vk connector actual?")
		utils.VkConnectors[user.Id] = vap.enterTheMatrix(user)
		return nil
	} else {
		return errors.New("Vk credentials not completed, disabling vk")
	}

}

//Возвращаем полный адрес файлов находящихсся по пути fpath

func (vap *VkApiProvider) PostGroupPhoto(user *psql.User, post *psql.Post) error {
	vap.logger.Info("posting to vk")
	groupip, err := strconv.Atoi(user.VKgroups)
	if err != nil {
		return err
	}
	photo, err := utils.VkConnectors[user.Id].UploadGroupWallPhotos(groupip, []string{post.PhotoPath})
	if err != nil {
		return err
	}
	params := url.Values{}
	params.Set("attachments", utils.VkConnectors[user.Id].GetPhotosString(photo))
	data, err := utils.VkConnectors[user.Id].WallPost(groupip, post.Text, params)
	if err != nil {
		return err
	}
	println("data")
	println(data)
	return nil

}

// функция которая получает id запощеной картники
func getPicId(p url.Values, u *psql.User) string {
	resp, err := http.Get(
		"https://api.vk.com/method/photos.saveWallPhoto?access_token=" +
			utils.VkConnectors[u.Id].Self.AccessToken +
			"&v=5.37&user_id=" +
			p.Get("user_id") +
			"&server=" +
			p.Get("server") +
			"&photo=" +
			p.Get("photo") +
			"&hash=" +
			p.Get("hash"))
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	stringbody := string(body)
	array := strings.Split(stringbody, ",")
	picid := strings.TrimLeft(array[2], "\"id\":")
	return picid
}

//func postSelfWallPhoto(photolist []string, sign string) {
//	//получаем имя серера на который можно постить картинку
//	//Необходимо сконфигурировать функцию, которая будет получат картинку на вход
//	params := url.Values{}
//	params.Set("user_id", fmt.Sprint(utils.VkConnector.Self.UID))
//	//uploadPhotoData, err := clnt.PhotoUpload(params, []string{"./e4fff450a6306e045f5c26801ce31c3efaeb.jpg"}, "photos.getWallUploadServer")
//	uploadPhotoData, err := utils.VkConnector.PhotoUpload(params, photolist, "photos.getWallUploadServer")
//	utils.VkConnector.PhotoUpload()
//	if err != nil {
//		log.Println(err)
//	}
//
//	//формируем запрос для получения id картинки
//	p := url.Values{}
//	p.Set("user_id", "141174997")
//	p.Set("photo", string(uploadPhotoData.Photo))
//	p.Set("server", strconv.Itoa(uploadPhotoData.Server))
//	p.Set("hash", uploadPhotoData.Hash)
//	picid := getPicId(p)
//
//	//полученный id картинки постим у себя на странице
//	//тут должна быть полноценная функция в которую будет прилетать картинка и подпись
//	p.Set("attachments", string("photo"+fmt.Sprint(utils.VkConnector.Self.UID)+"_"+picid))
//	utils.VkConnector.WallPost(utils.VkConnector.Self.UID, sign, p)
//
//}
