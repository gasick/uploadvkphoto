package psql

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"poster/utils"
)

func initDBConn() *gorm.DB {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%d sslmode=disable",
		utils.Env.POSTGRES_HOST,
		utils.Env.POSTGRES_USER,
		utils.Env.POSTGRES_PASS,
		utils.Env.POSTGRES_DB,
		utils.Env.POSTGRES_PORT,
	)
	log.Println(dsn)

	conn, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil || conn == nil {
		if err != nil {
			log.Fatal(err)
		}
	}
	return conn
}
func migrate(dbc *gorm.DB) {

	err := dbc.AutoMigrate(
		&Post{},
		&User{},
	)
	if err != nil {
		log.Println(err)
	}
}
