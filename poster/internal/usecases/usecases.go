package usecases

import (
	"context"
	"errors"
	"go.uber.org/zap"
	"poster/internal/dataproviders/psql"
	"poster/internal/dataproviders/telegram"
	"poster/internal/dataproviders/vk"
	"time"
)

type UseCases struct {
	ctx        context.Context
	postgresdb psql.IPostgresProvider
	logger     *zap.SugaredLogger
	telegram   telegram.ITelegramApiProvider
	vk         vk.IVkApiProvider
}

type IUseCases interface {
	PostsForPublication(time2 time.Time) *[]psql.Post
	PostToVk(user *psql.User, post *psql.Post) (bool, error)
	PostToTelegram(user *psql.User, post *psql.Post) (bool, error)
	CacheActuator(user *psql.User) error
}

func NewUseCases(
	ctx context.Context,
	postgresdb psql.IPostgresProvider,
	sugaredlogger *zap.SugaredLogger,
) IUseCases {
	return &UseCases{
		ctx:        ctx,
		postgresdb: postgresdb,
		logger:     sugaredlogger,
		telegram:   telegram.NewTelegramApiProvider(ctx, sugaredlogger),
		vk:         vk.NewVkApiProvider(ctx, sugaredlogger),
	}
}
func (ui *UseCases) CacheActuator(user *psql.User) error {
	ui.logger.Info("Updating cache for ", user.Login)
	user, err := ui.postgresdb.GetUser(user)
	if err != nil {
		ui.logger.Error(err)
	}
	ui.logger.Info("Check telegram auth")
	err = ui.telegram.Reconnector(user)
	if err != nil {
		ui.logger.Error(err)
	}
	ui.logger.Info("Check vk auth")
	err = ui.vk.Reconnector(user)
	if err != nil {
		ui.logger.Error(err)
	}
	return err
}

func (ui *UseCases) PostsForPublication(t time.Time) *[]psql.Post {
	posts, err := ui.postgresdb.GetAllPostsByTimeWithNoDone(t)
	ui.logger.Infof("Amount of posts: %d", len(*posts))
	if err != nil {
		ui.logger.Error(err)
		return nil
	}
	return posts
}

func (ui *UseCases) PostToVk(user *psql.User, post *psql.Post) (bool, error) {
	if user.VKuser == "" || user.VKpass == "" || user.VKgroups == "" {
		return false, errors.New("No vk auth data")
	}
	err := ui.vk.PostGroupPhoto(user, post)
	if err != nil {
		return false, err

	}
	return true, nil

}

// postToTelegram публикует пост в telegram
func (ui *UseCases) PostToTelegram(user *psql.User, post *psql.Post) (bool, error) {
	ui.logger.Info("Telegram posting")
	if user.TelegramToken == "" || user.TelegramGroups == "" {
		return false, errors.New("No tg auth data")
	}
	err := ui.telegram.PostMessage(user, post)
	if err != nil {
		return false, err
	}
	return true, nil
}

////проверяем что есть фото и подпись, если фото и подпись есть, тогда публикуем.
//func (iu *UseCases) CheckPhotoAndSignAndIfExistPostIt(path filedb.Path) []error {
//	var errs []error
//	log.Println("checking for ", path)
//	if _, err := os.Stat(path.Pic); err == nil {
//		log.Println("\tPosting: ")
//		content, err := ioutil.ReadFile(path.Txt)
//		if err != nil {
//			content = []byte("")
//		}
//		if utils.Env.EnableVk {
//			err := iu.vk.PostGroupPhoto(photolist(path.Pic), string(content))
//			errs = append(errs, err)
//
//		}
//		if utils.Env.EnableTelegram {
//			err := iu.telegram.PostMessage(photolist(path.Pic), string(content))
//			errs = append(errs, err)
//		}
//	}
//	return nil
//}
//
//func photolist(fpath string) []string {
//	if strings.Contains(fpath, "jpg") {
//		return []string{fpath}
//	}
//	var photoslist []string
//	files, _ := os.ReadDir(fpath)
//	path, _ := filepath.Abs(fpath)
//	for _, file := range files {
//		//fmt.Println(filepath.Join(path, file.Name()))
//		if !strings.Contains(file.Name(), "post.txt") {
//			photoslist = append(photoslist, filepath.Join(path, file.Name()))
//		}
//	}
//	log.Println("photoslist:")
//	log.Println(photoslist)
//	return photoslist
//}
//
