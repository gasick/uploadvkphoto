package utils

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	vkapi "github.com/himidori/golang-vk-api"
)

var (
	Env          EnvData
	TelegramBots map[int]*tgbotapi.BotAPI
	VkConnectors map[int]*vkapi.VKClient
)
